+++
title = "iJUG Open Source Scholarship, JAX-RS Contributions and more"
+++


Hey there! Long time no blog post from me.

### So what's new?

Actually, a lot of things.

First of all, I've been accepted for the [iJUG Open Source Scholarship](https://github.com/ijug-ev/Stipendium).

#### How did that happen?

A colleague of mine gave me a year-old copy of *[Java aktuell](https://www.ijug.eu/de/java-aktuell/zeitschrift/)* and pointed me to an article written by [Markus Karg](https://github.com/mkarg).
Basically, it said something along the lines of *If you don't contribute to Jakarta EE, who will?*.  
So I contacted Markus and asked him if they could use my help.  
And to my surprise, he said yes.  
Our correspondence went back and forth and Markus gave me some hints on where to start with my Open Source contributions.
Turns out we actually have a lot in common regarding our vision of how modern enterprise software should be written.

And then he told me that [iJUG](https://www.ijug.eu) has this scholarship to encourage people becoming [Committers](https://www.eclipse.org/membership/become_a_member/committer.php) to [Jakarta EE](https://jakarta.ee/), [MicroProfile](https://microprofile.io/) and [Adoptium](https://adoptium.net/).
Apart from some cool benefits like for example a ticket to [JavaLand](https://www.javaland.eu), the most valuable thing the scholarship provides is a mentor.
And Markus asked me if I could imagine becoming a fellow and his mentee - how cool is that??

All I had to do was making three commits to one of its member projects.  
At the time there was some big migration of the [TCK](https://en.wikipedia.org/wiki/Technology_Compatibility_Kit) of [Jakarta REST](https://github.com/jakartaee/rest) (formerly JAX-RS) going on where my help was much appreciated and the three commits where quickly accomplished.  
So that's how I became a fellow to the iJUG scholarship and a JAX-RS contributor!

> Pssst! Super hot tip:  
> Markus has a [YouTube Channel](https://www.youtube.com/user/headcrashing) where he makes funky videos about Java, JAX-RS and other cool stuff. You should definitely check it out!

#### What else did I do?

In JAX-RS, I did more work on the TCK, fixed some JavaDoc warnings, added some missing status codes and fixed a *file not found* error in the examplary jersey-tck module.

Since I have some interest in [Jakarta MVC](https://github.com/eclipse-ee4j/mvc-api), I had a look at that as well.
As a way for me to get comfy with it, I contributed a ViewEngine extension for Jinja Templates to its implementation [Krazo](https://github.com/eclipse-ee4j/krazo).

There's also [another (currently pending) PR](https://github.com/eclipse-ee4j/krazo/pull/291) to Krazo by me which reactivates the Thymeleaf ViewEngine extension which has been disabled due to Jakarta namespace incompatabilities.
