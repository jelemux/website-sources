+++
title = "Hello World!"
+++


Hello everyone! I'm Jeremias but in the internet I'm known (or not so known) as **`jelemux`** and this is my first post on this new and shiny blog!

**Let me tell you a little bit about myself.**

I'm a software developer from Germany currently at the end of my second year doing a three year vocational training. So if I'm a already a software developer depends on your definition. I do develop software, though!

**At work** I develop mainly in Java using JSF and Primefaces with a Maven workflow. I also do a lot of DevOps stuff, so I'm familiar with Jenkins, SonarQube, Vagrant, Ansible and Containers using Podman.  
We use Ubuntu VMs for development at work and I use Manjaro at home on my daily driver so I'm more than comfy using a variety of Linux distributions.

**Speaking of what I do at home...**

These are probably also the topics I will write about.

**Hobbies** (besides programming): I like hiking, mountainbiking and being outdoors in nature. I also used to go to the gym until Covid hit. I'm definitely planning on picking that up again.

**Rust**: I'm very interested in the language and have used it for a lot of my pet projects. And although I'm not yet as proficient in it as I'd like to be, I think it has some very cool features and a promising future.

**Quarkus**: Thinking about where the future of Java lies, I think this is the answer. Server side code is moving to scalable web services with microservice architecture and being reactive is gaining more and more importance. Frameworks like Quarkus and Spring make incorporating these principles easy and accesible.

**RISC-V**: Not much that I do here except reading and thinking. It's an open CPU architecture that can be used and modified by anyone without paying anything to big the big corporations that own the patents for all the other architectures. Sounds cool right? It seems to be such a thread to them that ARM even once tried launch a smear campaign against it.  
As you might have already guessed by now, I think about the future a lot. I (and apparently a whole bunch of other people as well) have come to the conclusion that RISC-V _is_ the future! It's designed to be highly versatile and the possibility to modify it greatly enhances that ability. It can be used for basically anything where a chip is needed: from small embedded applications over smartphones and desktops to servers and supercomputers. As a consumer I especially hope that it succeeds in the smartphone and desktop market.

**Ideas**: I usually tend to have a lot of ideas that could or could not make the future better.  
Just yesterday after visiting the Zeppelin Museum in Friedrichshafen I had the idea of putting solar panels on top of an airship to be used as a primary power supply for the engines. In an intense state of hyperfocus I even did some of the calculations. It could work - maybe.  
Another idea was a glider that could stay in the air forever by predicting rising air currents. Maybe they're useful, maybe they're not but I do have them and I like thinking about stuff like that.

I have a lot more interests but let's keep it at that for now. You'll hopefully hear from me again soon. Bye!
