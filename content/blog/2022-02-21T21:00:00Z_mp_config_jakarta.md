+++
title = "Using MicroProfile Config in the Jakarta Namespace"
+++

Since some time I've been wanting to write a post about my journey trying to add MicroProfile Config to a project using libraries in the Jakarta namespace.

MicroProfile Config API supports [CDI 3.0](https://mvnrepository.com/artifact/jakarta.enterprise/jakarta.enterprise.cdi-api/3.0.0) (which is the version where the namespace change came into effect) since [version 3.0](https://mvnrepository.com/artifact/org.eclipse.microprofile.config/microprofile-config-api/3.0) released in November 2021.  
So when I wanted to use it, it was basically possible but there was no CDI support because of namespace incompatabilites with the implementations.

But end of December 2021 SmallRye released a [Release Candidate](https://mvnrepository.com/artifact/io.smallrye.config/smallrye-config/3.0.0-RC1) that implements MP Config API 3.0 and therefore supports the Jakarta namespace.  
Yes, I know it's just a release candidate and not a final release, but it's good progress.
