+++
title = "I contributed to Open Source and so can you!"
+++

Yes, **you**! Don't be afraid, just do it!

> **Disclaimer:** This post is targeted towards developers that already have deeper knowledge of at least one programming language.
> You should also be comfortable with Git.  
> If that's not you but you want that to be you: Don't worry. Get started. Your journey will be awesome!

I my previous post I wrote about how I became a fellow to the iJUG scholarship and started contributing to Open Source.
Half a year ago the thought to do something like this would have overwhelmed me. Hell, thinking about it still feels overwhelming at times.

But the entry barrier was actually not as high as I thought.  
You might have the same fears as I did.
That's why I want to motivate you to contribute to Open Source.

And here's how you get started:

1. Find a piece of Open Source software that you like (may be something you use at work or something you use personally).
2. Go to the issue section of said project and look for issues labeled *good first issue* or *easy*.
3. If you think you can do it: Add a comment that you'd like to work on it.
4. If you don't know if you can do it: Clone the repo and try it! If you can do it, call dips.
5. Implement the issue and create a Pull Request.
6. People might suggest changes. This is completely normal and there's nothing bad about it - so embrace it!
7. Your PR gets merged and hurray, you're now officially an Open Source contributor!

That's it! Wasn't so hard, was it? Now go out and spread the word!
