+++
title = "About"
template = "about.html"
+++

Hey there!

My name is Jeremias Weber but in the world wide web I go by **`jelemux`**.  
I'm a software developer from Germany, coding Java during the day and Rust at night.

My interests are spread far and wide, including but not limited to Rust, Java, RISC-V, environmental protection and just the future in general.